#!/usr/bin/guile -s
!#

(use-modules (rnrs bytevectors)
             (ice-9 rdelim)
             (ice-9 regex)
             (srfi srfi-1)  ; fold
             (srfi srfi-11) ; let-values
             (srfi srfi-26) ; cut
             (srfi srfi-43) ; vector-for-each
             (web client)
             (web response)
             (json))

(set! *random-state* (random-state-from-platform))

(define base "http://localhost:8009/_matrix/client/r0")
(define base-clear "https://matrix.alsd.eu:8448/_matrix/client/r0")
(define bot-user "@repdigbot:alsd.eu")
(define bot-pass "<PASSWORD>")
(define admin-room "!btWQDbSQaYPLFDnhTI:alsd.eu")
(define verified-file "verified-users.txt")

(define auth-header (cons 'Authorization ""))
(define tx-id 0)
(define next-batch #f)

(define verified (make-hash-table 200))
(define challenges (make-hash-table)) ; user => (answer . time)
(define challenge-check-time 0)

(define invidious-instances '("redirect.invidious.io"))
(define teddit-instances '("teddit.net"))
(define nitter-instances
  '("nitter.net" "nitter.snopyta.org" "nitter.42l.fr" "nitter.nixnet.services"))

(define redirects
  (list (list (make-regexp "(youtube.com|youtu.be)/((watch\\?v=)?[^ ]+)") 2
              invidious-instances "Invidious")
        (list (make-regexp "reddit.com/([^ ]+)") 1
              teddit-instances "Teddit")
        (list (make-regexp "twitter.com/([^ ]+)") 1
              nitter-instances "Nitter")))

(define-syntax assoc-ref*
  (syntax-rules ()
    ((_ alist key key* ...)
     (assoc-ref* (assoc-ref alist key) key* ...))
    ((_ alist) alist)))

(define* (send-message room body #:optional (encrypted? #t))
  (http-put (format #f "~a/rooms/~a/send/m.room.message/~a"
                    (if encrypted? base base-clear) room tx-id)
            #:headers (list auth-header)
            #:body (scm->json-string
                     `((msgtype . "m.text")
                       ,@(if (pair? body)
                           `((body . ,(car body))
                             (format . "org.matrix.custom.html")
                             (formatted_body . ,(cdr body)))
                           `((body . ,body))))))

  (set! tx-id (1+ tx-id)))

(define* (sync #:optional (timeout 30000))
  (let*-values (((resp raw-body)
                 (http-get
                   (string-append
                     base "/sync?"
                     (if next-batch "since=" "")
                     (or next-batch "")
                     "&timeout=" (number->string timeout))
                   #:headers (list auth-header)))
                ((body) (json-string->scm (utf8->string raw-body))))

    (when (not (= 200 (response-code resp)))
      (error "Sync error:"
             (assoc-ref body "errcode")
             (assoc-ref body "error")))

    (set! next-batch (assoc-ref body "next_batch"))
    body))

(define (user-tag user)
  (format #f "<a href=\"https://matrix.to/#/~a\">~a</a>" user user))

(define (welcome-message user challenge)
  (cons
    (string-append
      "Benvenuto/a su Repressione Digitale, " user " :)\n"
       "Archivio condivisioni: https://shared02.opsone-cloud.ch/index.php/s/FonnRtE5psArG4i\n"
       "Come funziona Repressione Digitale ed Element desktop: #repressione-digitale-info:matrix.org\n"
       "Per verificare che non sei un bot, scrivi il risultato di " challenge)
    (format
      #f
      "Benvenuto/a su Repressione Digitale, ~a :)<br>
       Archivio condivisioni: <a href=\"https://shared02.opsone-cloud.ch/index.php/s/FonnRtE5psArG4i\">link</a><br>
       Come funziona Repressione Digitale ed Element desktop:
       <a href=\"https://matrix.to/#/#repressione-digitale-info:matrix.org\">#repressione-digitale-info:matrix.org</a><br>
       <b><i>Per verificare che non sei un bot, scrivi il risultato di ~a</i></b>"
       (user-tag user) challenge)))

(define (verified-message user)
  (cons
    (string-append
      "Grazie per la collaborazione " user ", buona permanenza :)")
    (string-append
      "Grazie per la collaborazione " (user-tag user) ", buona permanenza :)")))

(define (unanswered-message user)
  (cons
    (string-append user " non ha risposto")
    (string-append (user-tag user) " non ha risposto")))

(define (link-message link text)
  (cons
    (string-append text ": " link "\n")
    (string-append "<a href=" link ">" text "</a><br>")))

(define (join-messages a b)
  (cons (string-append (car a) (car b))
        (string-append (cdr a) (cdr b))))

(define (random-choose lst)
  (list-ref lst (random (length lst))))

(define (redirects-message message)
  (fold
    (lambda (redirect acc)
      (let ((regex (car redirect))
            (match-group (cadr redirect))
            (base (string-append "https://" (random-choose (caddr redirect))))
            (service-name (cadddr redirect)))
        (join-messages
          acc
          (fold-matches
            regex message '("" . "")
            (lambda (m acc)
              (join-messages
                acc
                (link-message
                  (string-append base "/" (match:substring m match-group))
                  (string-append "Link " service-name))))))))
    '("" . "")
    redirects))

(define (send-redirects room message)
  (let ((msg (redirects-message message)))
    (unless (string=? (car msg) "")
      (send-message room msg))))

(define (add-challenge user)
  (let ((a (1+ (random 10)))
        (b (1+ (random 10))))
    (hash-set! challenges user
               (cons (number->string (+ a b))
                     (current-time)))
    (format #f "~a + ~a" a b)))

(define (check-challenges)
  (let ((t (current-time))
        (expired '()))

   (hash-for-each
     (lambda (k v)
       (when (> (- t (cdr v)) 600)
         (send-message admin-room (unanswered-message k))
         (set! expired (cons k expired))))
     challenges)

   (for-each (cut hash-remove! challenges <>) expired)))

(define (add-to-verified user)
  (hash-set! verified user #t)
  (let ((out (open-file verified-file "a")))
    (display user out)
    (newline out)
    (close-port out)))

(define (handle-event event room)
  (let ((type (assoc-ref event "type"))
        (content (assoc-ref event "content"))
        (sender (assoc-ref event "sender")))

    (cond
      ((string=? sender bot-user))

      ((and (string=? type "m.room.member")
            (string=? (assoc-ref content "membership") "join")
            (not (equal? (assoc-ref* event "unsigned" "prev_content" "membership") "join"))
            (not (hash-ref verified sender))
            (not (hash-ref challenges sender)))
       (send-message room (welcome-message sender (add-challenge sender)) #f))

      ((string=? type "m.room.message")
       (let ((ch (hash-ref challenges sender))
             (body (assoc-ref content "body")))
         (send-redirects room body)

         (when (and ch (string-contains body (car ch)))
           (hash-remove! challenges sender)
           (send-message room (verified-message sender))
           (add-to-verified sender)))))))

;; load verified users
(when (not (file-exists? verified-file))
  (with-output-to-file verified-file
    (lambda ()
      (display bot-user)
      (newline))))

(with-input-from-file verified-file
  (lambda ()
    (let loop ((line (read-line)))
      (unless (eof-object? line)
        (hash-set! verified line #t)
        (loop (read-line))))))

;; log in
(let*-values (((resp raw-body)
               (http-post
                 (string-append base "/login")
                 #:body (scm->json-string
                          `((type . "m.login.password")
                            (identifier .
                                        ((type . "m.id.user")
                                         (user . ,bot-user)))
                            (password . ,bot-pass)
                            (device_id . "bot")))))
              ((body) (json-string->scm (utf8->string raw-body))))

  (when (not (= 200 (response-code resp)))
    (error "Login error:"
           (assoc-ref body "errcode")
           (assoc-ref body "error")))

  (set-cdr! auth-header
    (string-append
      "Bearer "
      (assoc-ref body "access_token"))))

;; drop old messages
(sync 0)

;; sync loop
(let loop ((state (sync)))
  (for-each
    (lambda (room)
      (vector-for-each
        (lambda (_ ev) (handle-event ev (car room)))
        (assoc-ref* (cdr room) "timeline" "events")))
    (assoc-ref* state "rooms" "join"))

  (when (> (- (current-time) challenge-check-time) 300)
    (check-challenges))

  (loop (sync)))
