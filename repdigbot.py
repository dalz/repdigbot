#!/usr/bin/env python3

from time import time
from nio import AsyncClient, RoomMessageText, RoomMemberEvent
import asyncio, nio, json, os, re, random
from aiohttp import ClientSession, ClientTimeout

HOMESERVER = "https://matrix.alsd.eu"
USER       = "repdigbot"
PASSWORD   = "<PASSWORD>"
ADMIN_ROOM = "!btWQDbSQaYPLFDnhTI:alsd.eu"

STORE_PATH = "store"
LOGIN_FILE = STORE_PATH + "/login.json"

STARTED_TS = time()

verified = ["@repdigbot:alsd.eu"]
challenges = {}

nitter_instances = (
    "nitter.net", "nitter.snopyta.org", "nitter.42l.fr", "nitter.nixnet.services"
)

teddit_instances = ("teddit.net",)

invidious_instances = set((
    "invidious.fdn.fr", "invidious.snopyta.org", "invidious.xyz", "invidious.tube"
))

async def redirect_invidious(match, session, tried=set()):
    fallback = len(tried) == len(invidious_instances)

    if fallback:
        instance = "invidio.us"
    else:
        instance = random.sample(invidious_instances.difference(tried), 1)[0]
        tried.add(instance)

    instance = "https://" + instance
    link = instance + "/" + match.groups()[-3]
    vid = match.groups()[-1]

    if fallback:
        return f'<a href="{link}">Link Invidious</a>'

    try:
        async with session.get(instance + "/api/v1/videos/" + vid) as resp:
            if resp.status == 200:
                if json := await resp.json():
                    if title := json.get("title"):
                        return f'<a href="{link}">{title} (Invidious)</a>'
    except asyncio.TimeoutError:
        await redirect_invidious(match, session, tried)

redirects = {
    re.compile(r'(\w+\.)?twitter\.com/(\S+)'): ("Nitter", nitter_instances),
    re.compile(r'(\w+\.)?reddit\.com/(\S+)'): ("Teddit", teddit_instances),
    re.compile(r'(\w+\.)?(youtube.com|youtu.be)/((watch\?v=)?(\w+)\S*)'): ("Invidious", redirect_invidious),
}

def user_tag(user_id):
    return f'<a href="https://matrix.to/#/{user_id}">{user_id}</a>'

def is_old_event(event):
    try:
        event_time = time() - event.source["unsigned"]["age"] / 1000
        return event_time < STARTED_TS
    except KeyError:
        return False

def welcome_text(user_id):
    a, b = random.randint(1, 50), random.randint(1, 50)
    challenges[user_id] = (f"{a} + {b}", a + b, time())

    return f'''Benvenuto/a su Repressione Digitale, {user_tag(user_id)} :)
    <br>
    Archivio condivisioni: https://shared02.opsone-cloud.ch/index.php/s/FonnRtE5psArG4i
    <br>
    Come funziona Repressione Digitale ed Element desktop:
    <a href="https://matrix.to/#/#repressione-digitale-info:matrix.org">#repressione-digitale-info:matrix.org</a>
    <br>
    <b><i>Per verificare che non sei un bot, scrivi il risultato di
    {challenges[user_id][0]}</i></b>'''

def html_to_plain(body):
    body = body.replace("\n", " ").replace("<br>", "\n")
    body = re.sub(r'<a href="https://matrix.to[^"]*">(.*?)</a>', r'\1', body)
    body = re.sub(r'<b>(.*?)</b>', r'**\1**', body)
    body = re.sub(r'<i>(.*?)</i>', r'*\1*', body)
    return re.sub(r'<a href="([^"]*)">(.*?)</a>', r'\2: \1', body)

async def send_msg(room_id, body):
    content = {
        "msgtype": "m.text",
        "format": "org.matrix.custom.html",
        "formatted_body": body,
        "body": html_to_plain(body)
    }

    await client.room_send(
        room_id = room_id,
        message_type="m.room.message",
        ignore_unverified_devices = True,
        content = content
    )

async def message_cb(room, event):
    if is_old_event(event):
        return

    if event.sender in challenges and str(challenges[event.sender][1]) in event.body:
        await send_msg(room.room_id, f"Grazie per la collaborazione {user_tag(event.sender)}, buona permanenza :)")
        verified.append(event.sender)
        del challenges[event.sender]

    links = []

    async with ClientSession(timeout = ClientTimeout(total=10)) as session:
        for pat, (service, urls) in redirects.items():
            for m in pat.finditer(event.body):
                if type(urls) is tuple:
                    link = "https://" + random.choice(urls) + "/" + m.groups()[-1]
                    links.append(f'<a href="{link}">Link {service}</a>')
                else:
                    links.append(await urls(m, session))

    if len(links):
        await send_msg(room.room_id, "<br>".join(links))


async def user_joined_cb(room, event):
    user_id = event.state_key

    if event.membership != "join" \
       or is_old_event(event) \
       or user_id in verified \
       or user_id in challenges.keys() \
       or event.prev_membership == "join":
        return

    await asyncio.sleep(3)
    await send_msg(room.room_id, welcome_text(user_id))

    try:
        if not len((await client.keys_query()).device_keys[user_id]):
            await send_msg(ADMIN_ROOM, f"{user_tag(user_id)} non ha sessioni attive")
    except Exception as e:
        print(e)


async def alert_loop():
    while True:
        await asyncio.sleep(300)

        now = time()
        to_be_deleted = []

        for user, (_, _, ts) in challenges.items():
            if now - ts > 600:
                await send_msg(ADMIN_ROOM, f"{user_tag(user)} non ha risposto")
                to_be_deleted.append(user)

        for user in to_be_deleted:
            del challenges[user]


async def main():
    global client

    client = nio.AsyncClient(
        homeserver=HOMESERVER,
        user=USER
        store_path=STORE_PATH,
        config=nio.AsyncClientConfig(store_sync_tokens=True)
    )

    if not os.path.exists(STORE_PATH):
        os.mkdir(STORE_PATH)

    if not os.path.exists(LOGIN_FILE):
        resp = await client.login(PASSWORD)
        print(resp)

        if not isinstance(resp, nio.LoginResponse):
            return

        with open(LOGIN_FILE, "w") as f:
            json.dump({
                "access_token": resp.access_token,
                "device_id": resp.device_id,
                "user_id": resp.user_id
            }, f)
    else:
        with open(LOGIN_FILE, "r") as f:
            client.restore_login(**json.load(f))
            print("Login restored")

    client.add_event_callback(user_joined_cb, RoomMemberEvent)
    client.add_event_callback(message_cb, RoomMessageText)

    asyncio.create_task(alert_loop())
    await client.sync_forever(timeout=30000, full_state=True)

asyncio.run(main())
