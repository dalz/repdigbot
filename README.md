# Repressione Digitale bot
Matrix bot for the (now defunct) Repressione Digitale italian community.
`repdigbot.py` is the first implementation with `matrix-nio`, `repdigbot.scm`
is the Guile rewrite, which uses the API directly. The bot greets new users and
asks them to solve a simple addition to verify they're human; if they don't
reply, a notification is sent to an admin room. It also converts youtube,
reddit, and twitter links to invidious, teddit and nitter. `repdigbot.scm`
needs pantalaimon to work in encrypted rooms, and sends the greeting
unencrypted to work around some issues we encountered.
